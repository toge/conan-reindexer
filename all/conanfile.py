from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import apply_conandata_patches, export_conandata_patches, get, rmdir
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
import os

required_conan_version = ">=1.53.0"

class ReindexerConan(ConanFile):
    name = "reindexer"
    description = "Embeddable, in-memory, document-oriented database with a high-level Query builder interface."
    license = "Apache-2.0"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/Restream/reindexer"
    topics = ("embeddable", "in-memory", "document-oriented", "database")
    package_type = "library"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "with_unwind": [True, False],
        "with_tcmalloc": [True, False],
        "with_jemalloc": [True, False],
        "with_rocksdb": [True, False],
        "with_grpc": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "with_unwind": True,
        "with_tcmalloc": False,
        "with_jemalloc": False,
        "with_rocksdb": False,
        "with_grpc": False,
    }

    @property
    def _min_cppstd(self):
        return 17

    @property
    def _compilers_minimum_version(self):
        return {
            "gcc": "7",
            "clang": "7",
            "apple-clang": "10",
            "Visual Studio": "16",
            "msvc": "192",
        }

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self, src_folder="src")

    def requirements(self):
        self.requires("snappy/1.1.10")
        self.requires("leveldb/1.23")
        self.requires("fmt/11.0.2", transitive_headers=True)
        if self.options.with_unwind:
            self.requires("libunwind/1.8.1")
        if self.options.with_grpc:
            self.requires("grpc/1.65.0")

    def validate(self):
        if self.settings.compiler.cppstd:
            check_min_cppstd(self, self._min_cppstd)
        minimum_version = self._compilers_minimum_version.get(str(self.settings.compiler), False)
        if minimum_version and Version(self.settings.compiler.version) < minimum_version:
            raise ConanInvalidConfiguration(
                f"{self.ref} requires C++{self._min_cppstd}, which your compiler does not support."
            )

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["ENABLE_LIBUNWIND"] = self.options.with_unwind
        tc.variables["ENABLE_GPRC"] = self.options.with_grpc
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rmdir(self, os.path.join(self.package_folder, "lib", "reindexer"))
        rmdir(self, os.path.join(self.package_folder, "lib", "pkgconfig"))

    def package_info(self):
        self.cpp_info.libs = ["reindexer", "friso_dict_resources"]
        self.cpp_info.includedirs.append(os.path.join("include", "reindexer"))
        self.cpp_info.set_property("pkg_config_name", "libreindexer")
        self.cpp_info.defines.append("REINDEX_CORE_BUILD")
        if self.settings.os == "Linux":
            self.cpp_info.system_libs.append("dl")
